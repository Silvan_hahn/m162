![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Steckbrief

Verfassen Sie einen Steckbrief über Ihre Person **auf Papier**. Machen Sie darin Aussagen über Ihre
Interessen an 

- Demographische Angaben (Alter, Geschlect, Name, etc)
- Computern/Informatik
- Ihre Privatinteressen (Musik, Videospiele, Sport, etc). 

Die Steckbriefe werden Ihre KollegenInnen später lesen können. 

#### Zeit und Form

15 Minuten

**Handschriftlich** auf A4 Blatt

---

&copy;TBZ, 2021, Modul: m162