![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Social Media III": Vom logischen zum physischen ERD

In dieser Übung wandeln sie ein logisches in ein physisches ERD um.

#### Aufgabe

Verwenden sie die Musterlösung aus der Übung "Social Media II" oder ihre eigene Lösung. Wandeln sie es in ein physisches Modell um.

**Tasks**:

- Erstellen sie das ERD mit MySql Workbench
- Erstellen sie alle Beziehungen
- Erstellen sie alle Attribute für die Tabellen
- Stellen sie sicher, dass alle Attribute, die korrekten Namen und Eigenschaften (FK, NN, UQ, FK) besitzen.

#### Zeit und Form

- 10 Minuten
- Individuell

---

&copy;TBZ, 2021, Modul: m162