![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag: Diagramme erklären

Schauen sie sich die Auswertung und die Diagramme ihrer Umfrage an, die vorher ausgefüllt wurde.

#### Aufgaben

Gehen sie alle Fragen durch und...

- ... erklären sie wieso der Diagrammtyp korrekt ist. Nehmen sie die [Präsentation](../Einsatz von Diagrammtypen.pdf) als Grundlage
- ... überlegen sie sich jeweils, ob auch andere Diagrammtypen in Frage kommen.

**Schreiben sie ihre Antworten auf**

#### Zeit und Form

- 30 Minuten
- Online Auswertung
- Individuell

---

&copy;TBZ, 2021, Modul: m162