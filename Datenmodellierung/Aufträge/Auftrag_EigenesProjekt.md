![TBZ Logo](../../x_gitressourcen/tbz_logo.png)

---

# Auftrag "Eigenes Projekt"

Definieren sie ein eigenes Projekt, das sie gerne als ERM/Datenbank abbilden möchten. Sie können ein beliebiges Thema in ihrem Interessengebiet nehmen.

#### Aufgabe

Ihr Projekt muss eine vernünftige Grösse haben. Erstellen sie die Projektbeschreibung und evtl. das konzeptionelle ERD und besprechen es mit der Lehrperson bevor sie weiterfahren.

**Tasks**:

- Erstellen sie eine Dokumentation mit
  - Projektbeschreibung
  - konzeptionelles ERM
  - physisches ERM
  - Beschreibung der einzelenen Tabellen
- Erstellen sie zuerst das konzeptionelle ERM
- Erstellen sie dann das physische ERM
- Die Aufteilung der Attribute in mehrere Tabellen muss dabei der 3. Normalform entsprechen.

#### Zeit und Form

- 3-4 Lektionen
- Individuell

---

&copy;TBZ, 2021, Modul: m162
